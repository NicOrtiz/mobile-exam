package com.example.nicolas.partidosfunx.Rest;


import com.google.gson.annotations.SerializedName;

/**
 * Created by Nicolas on 22-02-2018.
 */
//Serializar los datos api
public class Match {
    //imagen de equipo local
    @SerializedName("local_team_image_team-icon_width_i")
    private int localTeamImageIconWidth;

    //id unica
    @SerializedName("uuid_s")
    private String uuid;

    //nombre equipo visitante
    @SerializedName("visit_team_slug_s")
    private String visitTeamSlug;

    //id estadio
    @SerializedName("stadium_external_id_s")
    private String stadiumExternalId;

    //"nombre de campeonato"
    @SerializedName("competition_name_s")
    private String competitionName;

    //imagen del equipo local
    @SerializedName("local_team_image_team-icon_url_s")
    private String localTeamImageIconUrl;

    //altura imagen equipo local
    @SerializedName("local_team_image_team-icon_height_i")
    private int localTeamImageIconHeight;

    //d unica de equipo vistante
    @SerializedName("visit_team_uuid_s")
    private String visitTeamUuid;

    //goles de visita
    @SerializedName("visit_goals_i")
    private int visitGoals;


    //"id externa"
    @SerializedName("external_id_s")
    private String externalId;


    //ciudad del partido
    @SerializedName("stadium_city_name_s")
    private String stadiumCityName;


    //link compras de entradas "no disponible"
    @SerializedName("tickets_s")
    private String tickets;

    //id externa del equipo local
    @SerializedName("local_team_external_id_s")
    private String localTeamExternalId;

    //id
    @SerializedName("id")
    private String id;

    //ancho imagen de equipo visitante
    @SerializedName("visit_team_image_team-icon_width_i")
    private int visitTeamImageIconWidth;

    //id unica de estadio
    @SerializedName("stadium_uuid_s")
    private String stadiumUuid;

    //puntuación partido
    @SerializedName("score")
    private float resultado;

    //goles de penal del local
    @SerializedName("penalty_local_goals_i")
    private int penaltyLocalGoals;

    //altura de imagend el equipo visitante
    @SerializedName("visit_team_image_team-icon_height_i")
    private int visitTeamImageIconHeight;

    //version
    @SerializedName("_version_")
    private String version;

    //goles de local
    @SerializedName("local_goals_i")
    private int localGoals;


    //temperatura "no aplica"
    @SerializedName("temperature_s")
    private String temperature;

    //nombre periodo
    @SerializedName("period_name_s")
    private String periodName;

    //nombre de campeonato y nombre del partido
    @SerializedName("slug_s")
    private String slug;

    //nombre del campeonato
    @SerializedName("competition_slug_s")
    private String competitionSlug;

    //fecha del campeonato
    @SerializedName("match_day_s")
    private String fecha;

    //tipo de encuentro
    @SerializedName("type_s")
    private String type;

    //Hora inicio del partido
    @SerializedName("start_time_dt")
    private String startTime;

    //equipo al que pertenece la app ¿?
    @SerializedName("local_team_app_slug_s")
    private String localTeamAppSlug;

    //Nombre slug del Estadio a jugar
    @SerializedName("stadium_slug_s")
    private String stadiumSlug;

    //imagen del equipo visitante
    @SerializedName("visit_team_image_team-icon_url_s")
    private String visitTeamImageIconUrl;

    //Nombre del estadio a jugar
    @SerializedName("stadium_name_s")
    private String stadiumName;

    //Si el partido está confirmado
    @SerializedName("is_confirmed_s")
    private String isConfirmed;

    //Nombre slug del equipo local
    @SerializedName("local_team_slug_s")
    private String localTeamSlug;

    //equipo la que pertenece la app 2 ¿?
    @SerializedName("visit_team_app_slug_s")
    private String visitTeamAppSlug;


    //nombre del equipo visitante
    @SerializedName("visit_team_name_s")
    private String visitTeamName;


    //goles de penalty del equipo visitante
    @SerializedName("penalty_visit_goals_i")
    private int penaltyVisitGoals;


    //tiempo del partido ¿?
    @SerializedName("period_slug_s")
    private String periodSlug;


    //id unica del equipo local
    @SerializedName("local_team_uuid_s")
    private String localTeamUuid;

    //equipo la que pertenece la app 3 ¿?
    @SerializedName("app_slug_s")
    private String appSlug;

    //Nombre del equipo local
    @SerializedName("local_team_name_s")
    private String localTeamName;

    //id externa del equipo visitante
    @SerializedName("visit_team_external_id_s")
    private String visitTeamExternalId;

    public int getLocalTeamImageIconWidth()
    {
        return localTeamImageIconWidth;
    }

    public String getUuid()
    {
        return uuid;
    }

    public String getVisitTeamSlug()
    {
        return visitTeamSlug;
    }

    public String getStadiumExternalId()
    {
        return stadiumExternalId;
    }

    public String getCompetitionName()
    {
        return competitionName;
    }

    public String getLocalTeamImageIconUrl()
    {
        return localTeamImageIconUrl;
    }

    public int getLocalTeamImageIconHeight()
    {
        return localTeamImageIconHeight;
    }

    public String getVisitTeamUuid()
    {
        return visitTeamUuid;
    }

    public int getVisitGoals()
    {
        return visitGoals;
    }

    public String getExternalId()
    {
        return externalId;
    }

    public String getStadiumCityName()
    {
        return stadiumCityName;
    }

    public String getTickets()
    {
        return tickets;
    }

    public String getLocalTeamExternalId()
    {
        return localTeamExternalId;
    }

    public String getId()
    {
        return id;
    }

    public int getVisitTeamImageIconWidth()
    {
        return visitTeamImageIconWidth;
    }

    public String getStadiumUuid()
    {
        return stadiumUuid;
    }

    public float getScore()
    {
        return resultado;
    }

    public int getPenaltyLocalGoals()
    {
        return penaltyLocalGoals;
    }

    public int getVisitTeamImageIconHeight()
    {
        return visitTeamImageIconHeight;
    }

    public String getVersion()
    {
        return version;
    }

    public int getLocalGoals()
    {
        return localGoals;
    }

    public String getTemperature()
    {
        return temperature;
    }

    public String getPeriodName()
    {
        return periodName;
    }

    public String getSlug()
    {
        return slug;
    }

    public String getCompetitionSlug()
    {
        return competitionSlug;
    }

    public String getType()
    {
        return type;
    }

    public String numeroFecha() {return fecha; }

    public String getStartTime()
    {
        return startTime;
    }

    public String getLocalTeamAppSlug()
    {
        return localTeamAppSlug;
    }

    public String getStadiumSlug()
    {
        return stadiumSlug;
    }

    public String getVisitTeamImageIconUrl()
    {
        return visitTeamImageIconUrl;
    }

    public String getStadiumName()
    {
        return stadiumName;
    }

    public String getIsConfirmed()
    {
        return isConfirmed;
    }

    public String getLocalTeamSlug()
    {
        return localTeamSlug;
    }

    public String getVisitTeamAppSlug()
    {
        return visitTeamAppSlug;
    }

    public String getVisitTeamName()
    {
        return visitTeamName;
    }

    public int getPenaltyVisitGoals()
    {
        return penaltyVisitGoals;
    }

    public String getPeriodSlug()
    {
        return periodSlug;
    }

    public String getLocalTeamUuid()
    {
        return localTeamUuid;
    }

    public String getAppSlug()
    {
        return appSlug;
    }

    public String getLocalTeamName()
    {
        return localTeamName;
    }

    public String getVisitTeamExternalId()
    {
        return visitTeamExternalId;
    }

    public String GetYear()
    {
        return startTime.split("T")[0].split("-")[0];
    }

    public String GetMonth()
    {
        return startTime.split("T")[0].split("-")[1];
    }

    public String GetDay()
    {
        return startTime.split("T")[0].split("-")[2];
    }

    public String GetHour()
    {
        return startTime.split("T")[1].split(":")[0];
    }

    public String GetMinutes()
    {
        return startTime.split("T")[1].split(":")[1];
    }
}
