package com.example.nicolas.partidosfunx;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.CalendarContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.nicolas.partidosfunx.Rest.Match;
import com.facebook.drawee.view.SimpleDraweeView;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Nicolas on 22-02-2018.
 */

public class ViewAdapter extends ArrayAdapter<Match>
{
  private static class MatchHolder
    {
        //cargar vistas a utilizar
        public TextView fechaCampeonato;
        public TextView dateLocation;
        public SimpleDraweeView localTeamImage;
        public TextView localTeamName;
        public TextView resultado;
        public SimpleDraweeView visitTeamImage;
        public TextView visitTeamName;
        public LinearLayout calendar;
    }

    private Context context;
    //Meses
    private String[] monthNames = { "Enero", "Febrero", "Marzo", "Abr", "Mayo",
            "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" };


    public ViewAdapter(Context context, int resource, List<Match> matches)
    {
        super(context, resource, matches);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        final Match match = getItem(position);
        final MatchHolder holder;

        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        String fecha = "fecha";
        String stadiumName = "Stadium";

        if (convertView == null)
        {
            holder = new MatchHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.match_view, parent, false);
            holder.fechaCampeonato = convertView.findViewById(R.id.fechaCampeonato);
            holder.dateLocation = convertView.findViewById(R.id.dateLocation);
            holder.localTeamImage = convertView.findViewById(R.id.localTeamIcon);
            holder.localTeamName = convertView.findViewById(R.id.localTeamLabel);
            holder.resultado = convertView.findViewById(R.id.resultado);
            holder.visitTeamImage = convertView.findViewById(R.id.visitTeamIcon);
            holder.visitTeamName = convertView.findViewById(R.id.visitTeamLabel);
            holder.calendar = convertView.findViewById(R.id.calendarLayout);
            holder.calendar.setOnClickListener(new View.OnClickListener()
            {

                @Override
                public void onClick(View view)
                {
                    scheduleEvent(match);
                }
            });

            convertView.setTag(holder);
        }
        else
        {
            holder = (MatchHolder) convertView.getTag();
        }


        try
        {
            stadiumName = URLDecoder.decode(match.getStadiumName(), "UTF-8");
            fecha = URLDecoder.decode(match.numeroFecha());
        }
        catch (UnsupportedEncodingException e)
        {
            e.printStackTrace();
        }

        try
        {
            Date date = dateFormatter.parse(match.getStartTime());
            holder.dateLocation.setText(date.getDay() + " de " + monthNames[date.getMonth()] + ". - " + date.getHours() + "hrs - " + stadiumName);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }

        //poner el contenido en los "holder"
        holder.fechaCampeonato.setText(fecha);
        holder.localTeamImage.setImageURI(Uri.parse(match.getLocalTeamImageIconUrl()));
        holder.localTeamImage.getLayoutParams().height = match.getLocalTeamImageIconHeight();
        holder.localTeamImage.getLayoutParams().width = match.getLocalTeamImageIconWidth();

        holder.localTeamName.setText(match.getLocalTeamName());
        holder.resultado.setText(match.getLocalGoals() + " - " + match.getVisitGoals());

        holder.visitTeamImage.setImageURI(Uri.parse(match.getVisitTeamImageIconUrl()));
        holder.visitTeamImage.getLayoutParams().height = match.getVisitTeamImageIconHeight();
        holder.visitTeamImage.getLayoutParams().width = match.getVisitTeamImageIconWidth();

        holder.visitTeamName.setText(match.getVisitTeamName());

        return convertView;
    }


    //guardar en calendario
    private void scheduleEvent(Match match)
    {
        int year = Integer.parseInt(match.GetYear());
        int month = Integer.parseInt(match.GetMonth());
        int day = Integer.parseInt(match.GetDay());
        int startHour = Integer.parseInt(match.GetHour());
        int startMinutes = Integer.parseInt(match.GetMinutes());
        int endHour = startHour + 2;

        Calendar beginTime = Calendar.getInstance();
        beginTime.set(year, month, day, startHour, startMinutes);
        Calendar endTime = Calendar.getInstance();
        endTime.set(year, month, day, endHour, startMinutes);

        Intent intent = new Intent(Intent.ACTION_INSERT)
                .setData(CalendarContract.Events.CONTENT_URI)
                .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, beginTime.getTimeInMillis())
                .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTime.getTimeInMillis())
                .putExtra(CalendarContract.Events.TITLE, match.getLocalTeamName() + " vs " + match.getVisitTeamName())
                .putExtra(CalendarContract.Events.EVENT_LOCATION, match.getStadiumName())
                .putExtra(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY);
        context.startActivity(intent);
    }
}
