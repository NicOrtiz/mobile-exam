package com.example.nicolas.partidosfunx;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.nicolas.partidosfunx.Rest.GetMatchesResponse;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity
{
    //api a consumir
    private static final String END_POINT = "http://futbol.masfanatico.cl/api/u-chile/match/in_competition/chile-primera-2018/";
    private Context context;
    private RequestQueue requestQueue;
    private Gson gson;
    //listadapter
    private ViewAdapter matchAdapter;
    private ListView matches;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //inicializar fresco para las imagenes
        Fresco.initialize(this);
        //constructor de Gson para parsear
        GsonBuilder gsonBuilder = new GsonBuilder();
        gson = gsonBuilder.disableHtmlEscaping().create();
        context = this;

        requestQueue = Volley.newRequestQueue(this);
        GetMatches();
    }

    private void GetMatches()
    {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, END_POINT, onMatchesLoaded, onMatchesError);
        requestQueue.add(stringRequest);
    }

    private final Response.Listener<String> onMatchesLoaded = new Response.Listener<String>()
    {
        @Override
        public void onResponse(String response)
        {
            GetMatchesResponse matchesResponse = gson.fromJson(response, GetMatchesResponse.class);
            matchAdapter = new ViewAdapter(context, R.layout.match_view, Arrays.asList(matchesResponse.getMatches()));
            matches = findViewById(R.id.list_matches);
            matches.setAdapter(matchAdapter);
        }
    };
    //error
    private final Response.ErrorListener onMatchesError = new Response.ErrorListener()
    {
        @Override
        public void onErrorResponse(VolleyError error)
        {
            Log.e("MainActivity", error.toString());
        }
    };
}
