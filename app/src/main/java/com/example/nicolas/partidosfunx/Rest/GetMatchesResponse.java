package com.example.nicolas.partidosfunx.Rest;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Nicolas on 22-02-2018.
 */

public class GetMatchesResponse
{
    @SerializedName("has_next")
    private boolean hasNext;

    @SerializedName("rows_per_page")
    private int rowsPerPage;

    @SerializedName("num_pages")
    private int numPages;

    @SerializedName("items")
    private Match[] matches;

    @SerializedName("start_0_indexed")
    private int start0Indexes;

    @SerializedName("next_page")
    private int nextPage;

    @SerializedName("end_index")
    private int endIndex;

    @SerializedName("num_found")
    private int numFound;

    @SerializedName("generated_at")
    private int generatedAt;

    @SerializedName("has_previous")
    private boolean hasPrevious;

    @SerializedName("page")
    private int page;

    @SerializedName("start_index")
    private int startIndex;

    public boolean getHasNext()
    {
        return hasNext;
    }

    public int getRowsPerPage()
    {
        return rowsPerPage;
    }

    public int getNumPages()
    {
        return numPages;
    }

    public Match[] getMatches()
    {
        return matches;
    }

    public int getStart0Indexes()
    {
        return start0Indexes;
    }

    public int getNextPage()
    {
        return nextPage;
    }

    public int getEndIndex()
    {
        return endIndex;
    }

    public int getNumFound() {
        return numFound;
    }

    public int getGeneratedAt()
    {
        return generatedAt;
    }

    public boolean getHasPrevious()
    {
        return hasPrevious;
    }

    public int getPage()
    {
        return page;
    }

    public int getStartIndex()
    {
        return startIndex;
    }
}
